#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, ModelSQL
from trytond.transaction import Transaction


class Product(ModelSQL, ModelView):
    _name = "product.product"

    def products_by_location(self, location_ids, product_ids=None,
            with_childs=False, skip_zero=True):
        if Transaction().context.get('show_zero'):
            skip_zero = False
        return super(Product, self).products_by_location(location_ids,
                product_ids=product_ids, with_childs=with_childs,
                skip_zero=skip_zero)

Product()
