#This file is part of Tryton. The COPYRIGHT file at the top level of
#this repository contains the full copyright notices and license terms.
from trytond.model import ModelView, fields
from trytond.wizard import Wizard
from trytond.pyson import PYSONEncoder
from trytond.pool import Pool


class ChooseStockDateInit(ModelView):
    _name = 'stock.location_stock_date.init'

    show_zero = fields.Boolean('Show Zero Quantity')

    def default_show_zero(self):
        return False

ChooseStockDateInit()


class OpenProduct(Wizard):
    _name = 'stock.product.open'

    def _action_open_product(self, data):
        model_data_obj = Pool().get('ir.model.data')
        act_window_obj = Pool().get('ir.action.act_window')

        model_data_ids = model_data_obj.search([
            ('fs_id', '=', 'act_product_by_location_zero'),
            ('module', '=', 'stock_location_show_zero_quantity'),
            ('inherit', '=', False),
            ], limit=1)
        model_data = model_data_obj.browse(model_data_ids[0])
        res = act_window_obj.read(model_data.db_id)

        pyson_context = {}
        pyson_context['locations'] = data['ids']
        if data['form']['forecast_date']:
            pyson_context['stock_date_end'] = data['form']['forecast_date']
        else:
            pyson_context['stock_date_end'] = datetime.date.max

        if not data['form']['show_zero']:
            res['domain'] = "['OR', ('quantity', '!=', 0.0), " \
                    "('forecast_quantity', '!=', 0.0)]"

        res['pyson_context'] = PYSONEncoder().encode(pyson_context)

        return res

OpenProduct()
